<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Person extends Model
{
    use HasFactory;

    protected $table = 'persons';

    protected $primaryKey = 'client_id';

    public $incrementing = false;

    protected $fillable = ['client_id', 'date_of_birth'];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}

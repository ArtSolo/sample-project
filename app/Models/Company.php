<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Company extends Model
{
    use HasFactory;

    protected $primaryKey = 'client_id';

    public $incrementing = false;

    protected $fillable = ['client_id', 'registration_number'];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}

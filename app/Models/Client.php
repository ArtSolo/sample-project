<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['type_id', 'company_name', 'primary_contact_name'];

    public function person(): HasOne
    {
        return $this->hasOne(Person::class);
    }

    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }

    public function companyEmails(): HasMany
    {
        return $this->hasMany(CompanyEmail::class);
    }

    public function primaryContactEmails(): HasMany
    {
        return $this->hasMany(PrimaryContactEmail::class);
    }
}

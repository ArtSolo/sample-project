<?php

namespace App\Enums;

enum Type: int
{
    case Person = 1;
    case Company = 2;
}
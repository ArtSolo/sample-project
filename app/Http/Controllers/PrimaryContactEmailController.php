<?php

namespace App\Http\Controllers;

use App\Models\PrimaryContactEmail;
use Illuminate\Http\Request;

class PrimaryContactEmailController extends Controller
{
    public function store(Request $request) {
        $validatedData = $request->validate([
            'client_id' => 'required|integer',
            'value' => 'required|email'
        ]);
            
        PrimaryContactEmail::create($validatedData);

        return redirect()->route("clients.edit", ['id' => $request->client_id]);
    }

    public function destroy(int $id) {
        PrimaryContactEmail::destroy($id);
    }
}

<?php

namespace App\Http\Controllers;

use App\Enums\Type;
use App\Http\Requests\ClientCreateRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Models\Client;
use App\Models\Company;
use App\Models\CompanyEmail;
use App\Models\Person;
use App\Models\PrimaryContactEmail;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index(Request $request) {
        return inertia('Clients/Index', [
            'clients' => Client::select()
                ->offset(($request->query('page', 1) * 10) - 10)
                ->limit(10)
                ->get(),
            'pagesCount' => ceil(Client::count() / 10),
            'currentPage' => (int)$request->query('page', 1)
        ]);
    }

    public function show(int $id) {
        return inertia('Clients/Edit', [
            'client' => Client::where('id', $id)
                ->with('person')
                ->with('company')
                ->with('companyEmails')
                ->with('primaryContactEmails')
                ->first()
        ]);
    }

    public function destroy(int $id) {
        Client::destroy($id);

        return redirect()->route('clients');
    }

    public function store(ClientCreateRequest $request) {
        $validatedData = $request->validated();

        $client = Client::create($validatedData);

        if ($client->type_id === Type::Company->value) {
            Company::create([
                'client_id' => $client->id,
                'registration_number' => $validatedData['company']['registration_number']
            ]);
        } else {
            Person::create([
                'client_id' => $client->id,
                'date_of_birth' => $validatedData['person']['date_of_birth']
            ]);
        }

        if (isset($validatedData['company_emails'])) {
            foreach($validatedData['company_emails'] as $email) {
                CompanyEmail::create([
                    'client_id' => $client->id,
                    'value' => $email
                ]);
            }
        }

        if (isset($validatedData['primary_contact_emails'])) {
            foreach($validatedData['primary_contact_emails'] as $email) {
                PrimaryContactEmail::create([
                    'client_id' => $client->id,
                    'value' => $email
                ]);
            }
        }

        return redirect()->route('clients');
    }

    public function update(ClientUpdateRequest $request, int $id) {
        $client = Client::where('id', $id)->first();

        $validatedData = $request->validated();

        Client::where('id', $id)->update([
            'company_name' => $validatedData['company_name'],
            'primary_contact_name' => $validatedData['primary_contact_name']
        ]);

        if ($client->type_id === Type::Person->value) {
            Person::where('client_id', $id)->update([
                'date_of_birth' => $validatedData['person']['date_of_birth']
            ]);
        } else {
            Company::where('client_id', $id)->update([
                'registration_number' => $validatedData['company']['registration_number']
            ]);
        }

        return redirect()->route('clients');
    }
}

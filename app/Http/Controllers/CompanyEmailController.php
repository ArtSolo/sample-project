<?php

namespace App\Http\Controllers;

use App\Models\CompanyEmail;
use Illuminate\Http\Request;

class CompanyEmailController extends Controller
{
    public function store(Request $request) {
        $validatedData = $request->validate([
            'client_id' => 'required|integer',
            'value' => 'required|email'
        ]);
            
        CompanyEmail::create($validatedData);

        return redirect()->route("clients.edit", ['id' => $request->client_id]);
    }

    public function destroy(int $id) {
        CompanyEmail::destroy($id);
    }
}

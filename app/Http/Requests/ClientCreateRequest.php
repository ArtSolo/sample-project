<?php

namespace App\Http\Requests;

use App\Enums\Type;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClientCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(Request $request): array
    {
        return [
            'company_name' => 'required|max:100',
            'primary_contact_name' => 'required|max:100',
            'type_id' => ['required', Rule::enum(Type::class)],
            'company.registration_number' => [
                Rule::requiredIf($request->input('type_id') === Type::Company->value),
                Rule::when($request->input('type_id') === Type::Company->value, 'digits:6')
            ],
            'person.date_of_birth' => [
                Rule::requiredIf($request->input('type_id') === Type::Person->value),
                Rule::when($request->input('type_id') === Type::Person->value, 'date')
            ],
            'company_emails' => 'nullable|array',
            'company_emails.*' => 'required|email',
            'primary_contact_emails' => 'nullable|array',
            'primary_contact_emails.*' => 'required|email'
        ];
    }
}

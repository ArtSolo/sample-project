<?php

namespace App\Http\Requests;

use App\Enums\Type;
use App\Models\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(Request $request): array
    {
        $client = Client::where('id', $request->route('id'))->first();

        return [
            'company_name' => 'required|max:100',
            'primary_contact_name' => 'required|max:100',
            'company.registration_number' => [
                Rule::requiredIf($client->type_id === Type::Company->value),
                Rule::when($client->type_id === Type::Company->value, 'digits:6')
            ],
            'person.date_of_birth' => [
                Rule::requiredIf($client->type_id === Type::Person->value),
                Rule::when($client->type_id === Type::Person->value, 'date')
            ]
        ];
    }
}

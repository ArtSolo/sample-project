<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\CompanyEmailController;
use App\Http\Controllers\PrimaryContactEmailController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/clients', [ClientController::class, 'index'])->name('clients');
    Route::post('/clients', [ClientController::class, 'store'])->name('clients.store');
    Route::inertia('clients/create', 'Clients/Create')->name('clients.create');
    Route::patch('/clients/{id}', [ClientController::class, 'update'])->name('clients.update');
    Route::get('/clients/{id}', [ClientController::class, 'show'])->name('clients.edit');
    Route::delete('/clients/{id}', [ClientController::class, 'destroy'])->name('clients.delete');

    Route::post('/primary-contact-emails', [PrimaryContactEmailController::class, 'store'])->name('primary-contact-emails.store');
    Route::delete('/primary-contact-emails/{id}', [PrimaryContactEmailController::class, 'destroy'])->name('primary-contact-emails.delete');

    Route::post('/company-emails', [CompanyEmailController::class, 'store'])->name('company-emails.store');
    Route::delete('/company-emails/{id}', [CompanyEmailController::class, 'destroy'])->name('company-emails.delete');
});

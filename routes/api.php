<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\CompanyEmailController;
use App\Http\Controllers\PrimaryContactEmailController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('clients', ClientController::class)->except([
    'edit', 'create'
]);

Route::resource('company-emails', CompanyEmailController::class)->only([
    'store', 'update', 'destroy'
]);

Route::resource('primary-contact-emails', PrimaryContactEmailController::class)->only([
    'store', 'update', 'destroy'
]);

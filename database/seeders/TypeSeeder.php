<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('types')->upsert([[
            'id' => 1,
            'name' => 'person',
        ], [
            'id' => 2,
            'name' => 'company',
        ]], 'id');
    }
}

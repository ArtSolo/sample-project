<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Company;
use App\Models\CompanyEmail;
use App\Models\Person;
use App\Models\PrimaryContactEmail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Enums\Type;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $clients = Client::factory()->count(25)->create();

        foreach ($clients as $client) {
            if ($client->type_id === Type::Person->value) {
                Person::factory()->count(1)->state(['client_id' => $client->id])->create();
            } else {
                Company::factory()->count(1)->state(['client_id' => $client->id])->create();
            }
            
            CompanyEmail::factory()->count(fake()->numberBetween(1, 3))->state(['client_id' => $client->id])->create();

            PrimaryContactEmail::factory()->count(fake()->numberBetween(1, 3))->state(['client_id' => $client->id])->create();
        }
    }
}
